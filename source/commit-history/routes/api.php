<?php

use App\Http\Controllers\CommitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('/commits', [CommitController::class, 'store']);

//moved to web route.
//Route::get('/commits', [CommitController::class, 'index']);

