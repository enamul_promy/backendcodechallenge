<?php

namespace App\Providers;

use App\Models\GithubCommit;
use App\Repositories\Contracts\GithubCommitsRepository;
use App\Repositories\EloquentGithubCommtRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GithubCommitsRepository::class, function() {
            return new EloquentGithubCommtRepository(new GithubCommit());
        });

    }
}
