<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource as JsonResource;

class CommitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'commit_details' => $this->commit_details,
        ];
    }
}
