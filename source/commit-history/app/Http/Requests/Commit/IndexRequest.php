<?php

namespace App\Http\Requests\Commit;

use App\Http\Requests\Request;

class IndexRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'host' => 'in:github,bitbucket,gitlab'
        ];
    }

}
