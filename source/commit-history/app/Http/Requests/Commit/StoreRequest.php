<?php

namespace App\Http\Requests\Commit;

use App\Http\Requests\Request;

class StoreRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'owner' => '',
            'repo' => '',
            'host' => 'in:github,bitbucket,gitlab'
        ];
    }

}
