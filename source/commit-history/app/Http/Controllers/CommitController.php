<?php

namespace App\Http\Controllers;

use App\Http\Requests\Commit\IndexRequest;
use App\Http\Requests\Commit\StoreRequest;
use App\Http\Resources\CommitResourceCollection;
use App\Repositories\Contracts\GithubCommitsRepository;
use App\Services\GithubService;


class CommitController extends Controller
{

    protected $githubService;

    public function __construct(GithubService $githubService)
    {
        $this->githubService = $githubService;
    }

    public function store(StoreRequest $request)
    {
        $host = $request->get('host', 'github');
        $owner = $request->get('owner', 'nodejs');
        $repo = $request->get('repo', 'node');
        if($host == 'github') {
            $this->githubService->fetchAndSaveCommits($owner,$repo);
        } elseif ($host == 'bitbucket') {
            // fetch bitbucket commits from db
        } elseif ($host == 'gitlab') {
            // fetch gitlab commits from db
        } else {
            die();
        }
        return response()->json('ok', 201);
    }

    public function index(IndexRequest $request, GithubCommitsRepository $githubCommitsRepository)
    {
        $host = $request->get('host', 'github');
        $commits = [];
        if($host == 'github') {
            $commits = $githubCommitsRepository->findBy();
        } elseif ($host == 'bitbucket') {
            // fetch bitbucket commits from db
        } elseif ($host == 'gitlab') {
            // fetch gitlab commits from db
        } else {
            die();
        }
        //return new CommitResourceCollection($commits);
        return view('commits', ['commits' => $commits]);
    }

}
