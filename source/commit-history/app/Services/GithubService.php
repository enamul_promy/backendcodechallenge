<?php

namespace App\Services;

use App\Repositories\Contracts\GithubCommitsRepository;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;


class GithubService
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $headers;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('GITHUB_API_URL')]);
        $this->headers = [
            'Content-Type'  => 'application/json',
            'Accept' => 'application/json',
        ];
    }

    public function fetchAndSaveCommits($owner, $repo, $limit=25)
    {
        $requestBody = [
            'headers' => $this->headers,
        ];
        $data = json_decode($this->client->get("repos/{$owner}/${repo}/commits?per_page=${limit}", $requestBody)->getBody()->getContents());
        foreach ($data as $commit) {
            app(GithubCommitsRepository::class)->updateOrCreate(['commit_details.sha' => $commit->sha ],['commit_details' =>$commit]);
        }

    }

}
