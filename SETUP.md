### Setup instructions

- cd source/commit-history/
- copy and paste .env.example and then rename to .env  
- docker-compose up
- open another terminal and run the following command  
- docker exec -it app_commit_history bash
- composer install
- php artisan migrate  
- go to http://127.0.0.1:8087/ on a browser to see the API documentation.

Project should be running on port 8089. If docker compose shows any errors please check if all the required port are available.

Api to fetch and store commits: POST 127.0.0.1:8089/api/commits/

~~NOTE: I only implemented backend portion of the challenge.~~
Url to see frontend: http://127.0.0.1:8089/commits