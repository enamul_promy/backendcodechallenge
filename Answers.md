### Answers to Implementation and Conceptual Questions

_How were you debugging this mini-project? Which tools?_

Answer: I used dump and die method dd() of laravel framework. For more complex project I sometime use XDEBUG.


_How were you testing the mini-project?_

Answer: I tested this project using postman. I used to use phpunit for automated test. 

_Imagine this mini-project needs microservices with one single database, how would you draft an architecture?_

Answer: In opinion it will make sense to have one microservice(we could name it data-service) which will handle and maintain the communication with the database.
All other microservices that needs to access the database will do so via API's provided by the first microservice(data-service)

_How would your solution differ when all over the sudden instead of saving to a Database you would have to call another external API to store and receive the commits._

Answer: I already have an api to fetch and store the data into a database. We can just remove the storing from the code and return the response we receive from the external API.